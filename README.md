# GitLab Backed Algorithm Using CI/CD to Deploy to Algorithmia
To deploy your algorithm files to Algorithmia everytime you push to this repository, make sure you have the following configured. 

## Algorithm Files
As in your default algorithm repo template, your `src` folder and `requirements.txt` are in the root level of the repository. 

## Gitlab CI file and Algorithmia CI scripts
Make sure you have the following at the root level of this repo:
- `.gitlab-ci.yml` file 
- `ci_entrypoint.py` file, as the entrypoint to your Algorithmia Deployment CI jobs
- `algorithmia_ci` module directory, containing the scripts for deploying to Algorithmia and publishing a new version after runnning your defined tests

## Gitlab CI Environment Variables


## Test Case Files
If you want to run tests before publishing a new version of your algorithm, you should add a new file called `TEST_CASES.json` at the root level of this repository. If you do not provide this file, then the CI/CD pipeline will omit the testing step.
The required schema of this json file is shown below:

# Case Schema
Your test cases should follow the following json schema
```
[
 { 
    "case_name": String,
    "input": Any,
    "expected_output": Any,
    "type": String,
    "tree": List
  },
  ...
]
```

- `input` (required) - the raw input that will be passed into the algorithm. Typically this will be a json dictionary, json list, or a primitive type (like a string).
- `expected_output` (required) - What we are comparing against the result of your algorithm, which can be scoped in conjunction with setting `tree`. 
For types `GREATER_OR_EQUAL` and `LESS_OR_EQUAL` this must be a number value. For types `NO_EXCEPTION` and `EXCEPTION` this field is optional.
- `type` (optional) - defines the type of matching that can be done, options include `EXACT_MATCH`, `GREATER_OR_EQUAL`, `LESS_OR_EQUAL`, `NO_EXCEPTION` and `EXCEPTION`. Defaults to `EXACT_MATCH`
- `tree` (optional) - A list defining the json keys we should traverse in order to find the value you wish to compare against with `expected_output`.

## Example Case
```json
[
    {
      "case_name": "image_classifier_accuracy",
      "input": {"image_data":  [...]},
      "expected_output": 0.7,
      "type": "GREATER_OR_EQUAL",
      "tree": ["accuracy"],
    }
]
```
