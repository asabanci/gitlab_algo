import Algorithmia

# API calls will begin at the apply() method, with the request body passed as 'input'
# For more details, see algorithmia.com/developers/algorithm-development/languages
def apply(input):
    if isinstance(input, str):
        return "hello {}".format(input)
    elif isinstance(input, dict):
        model_input = input["text_to_analyze"]
        print(model_input)
        # Mock as if we have a model processing the input and returning a prediction
        pred = 0.69
        return {"prediction": pred}

